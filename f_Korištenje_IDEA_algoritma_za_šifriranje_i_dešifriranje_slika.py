from Crypto.Cipher import IDEA
from PIL import Image

def generate_key():
key = get_random_bytes(16)
return key
def encrypt_image(file_path, key):
with Image.open(file_path) as img:

        data = img.tobytes()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    padded_data = data + ((8 - len(data) % 8) * b'\x08')

    encrypted_data = cipher.encrypt(padded_data)

    with open(file_path + ".enc", 'wb') as file:

        file.write(encrypted_data)

def decrypt_image(file_path, key):

    with open(file_path, 'rb') as file:

        cipher_data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    decrypted_data = cipher.decrypt(cipher_data)

    unpadded_data = decrypted_data[:-decrypted_data[-1]]

    with open(file_path[:-4], 'wb') as file:

        file.write(unpadded_data)

    with Image.open(file_path[:-4]) as img:

        img.show()


key = generate_key()
file_path = "test.png"
encrypt_image(file_path, key)
decrypt_image(file_path + ".enc", key)
