Python 3.10.1 (tags/v3.10.1:2cd268a, Dec  6 2021, 19:10:37) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
from Crypto.Cipher import IDEA

from Crypto.Random import get_random_bytes

def generate_key():

    key = get_random_bytes(16)

    return key.hex()

key = generate_key()

print(key)