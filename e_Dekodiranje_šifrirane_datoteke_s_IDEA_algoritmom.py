from Crypto.Cipher import IDEA

def decrypt_file(file_path, key):

    with open(file_path, 'rb') as file:

        cipher_data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    decrypted_data = cipher.decrypt(cipher_data)

    unpadded_data = decrypted_data[:-decrypted_data[-1]]

    with open(file_path[:-4], 'wb') as file:

        file.write(unpadded_data)

key = b'\xd2z\xf2\x1f\xb8\x8d\xae\xc7\xdc\xba\xf9\x9aQy'

file_path = "test.txt.enc"

decrypt_file(file_path, key)
