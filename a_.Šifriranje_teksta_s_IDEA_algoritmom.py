Python 3.10.1 (tags/v3.10.1:2cd268a, Dec  6 2021, 19:10:37) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
from Crypto.Cipher import IDEA
from Crypto.Util.Padding import pad
def encrypt_text(text, key):
cipher = IDEA.new(key.encode(), IDEA.MODE_ECB)
padded_text = pad(text.encode(), 8)
encrypted_text = cipher.encrypt(padded_text)
return encrypted_text
text = "Ovo je tajni tekst"
key = "tajni_kljuc123"
encrypted_text = encrypt_text(text, key)
print(encrypted_text)